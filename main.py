#!/usr/local/bin/python3

from __future__ import print_function

import os
os.environ["KIVY_WINDOW"] = "sdl2"

from kivy.config import Config

#Config.read('config.txt')

from kivy.app import App
from kivy.uix.screenmanager import ScreenManager
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.widget import Widget
from kivy.uix.image import Image
from kivy.uix.label import Label
from kivy.graphics.texture import Texture
from kivy.graphics import Color, Rectangle
from kivy.core.window import Window
from kivy.clock import Clock
from kivy.properties import StringProperty
import kivy.metrics as metrics

from wordpress_xmlrpc import Client, WordPressPost, WordPressPostType
from wordpress_xmlrpc.compat import xmlrpc_client
from wordpress_xmlrpc.methods import media, posts, taxonomies
from wordpress_xmlrpc import WordPressTerm

import string
import time
from time import sleep
import re
import uuid
from os import listdir
from os.path import isfile, join

import imutils
import cv2 as cv2
import numpy as np
import shutil
from threading import Thread
import yaml

from picamera2 import Picamera2

config = yaml.safe_load(open("config.yml"))

LED_GPIO_PIN_NUM = config['led_gpio']

try:
    import RPi.GPIO as GPIO
    GPIO.setmode(GPIO.BCM)
    GPIO.setwarnings(False)
    GPIO.setup(LED_GPIO_PIN_NUM,GPIO.OUT)
except ImportError:
    pass

WINDOW_RES = (config['window_w'],config['window_h'])

# CV2_DIR = "/usr/local/Cellar/opencv3/HEAD-a212026_1/share/OpenCV/"
CV2_DIR = config['opencv_dir']
FACE_CASCADE = cv2.CascadeClassifier(CV2_DIR + config['face_cascade'])
EYE_CASCADE = cv2.CascadeClassifier(CV2_DIR + config['eye_cascade'])
NOSE_CASCADE = cv2.CascadeClassifier(CV2_DIR + config['nose_cascade'])

OVERLAYS_DIR = os.path.abspath(os.path.dirname(__file__)) + "/Overlays"
OVERLAYS = sorted(list(filter(lambda f: not f.startswith('.') and not "thumb" in f, 
                              [f for f in listdir(OVERLAYS_DIR) if isfile(join(OVERLAYS_DIR, f))])))

IMAGE_PREVIEW_W = config['image_preview_w']
IMAGE_PREVIEW_H = config['image_preview_h']

OUTPUT_IMG_W = config['output_img_w']
OUTPUT_IMG_H = config['output_img_h']

WP_URI = config['wordpress_uri']
WP_USER = config['wordpress_user']
WP_PWD = config['wordpress_pwd']

BLIT_REFRESH_INTERVAL = config['blit_refresh_interval']

CAMERA_INDEX = config['camera_index']

class CV2Camera(Widget):    
    def __init__(self, **kwargs):
        super(CV2Camera, self).__init__(**kwargs)
        
        print("OpenCV is version: " + cv2.__version__)
        
        # Initialize picamera2
        try:
            self.picam2 = Picamera2()
            self.picam2.configure(self.picam2.create_preview_configuration(main={"size": (IMAGE_PREVIEW_W, IMAGE_PREVIEW_H), "format": "RGB888"}))
            self.picam2.start()
            print("Picamera2 started successfully.")
        except Exception as e:
            print(f"Failed to initialize Picamera2: {e}")
            sys.exit(1)
        
        # Schedule the update method to be called regularly
        Clock.schedule_interval(self.update, BLIT_REFRESH_INTERVAL)
        
        self.overlay_index = 0
        
        # Load the initial frame
        frame = self.capture_frame()
        if frame is None:
            print("Error: Cannot capture initial frame from camera")
            sys.exit(1)
        
        # Apply initial processing
        self.img = self.process_frame(frame)
        
        # Apply initial overlay
        self.set_overlay_image(self.overlay_index)
        
        # Apply overlay to image
        self.apply_overlay()
        
        # Convert image to bytes
        self.buf = self.img.tobytes()
        
        # Create texture and rectangle
        with self.canvas:
            self.video_texture = Texture.create(size=(self.img.shape[1], self.img.shape[0]), colorfmt='rgb')
            self.video_texture.blit_buffer(self.buf, colorfmt='rgb', bufferfmt='ubyte')
            self.rect = Rectangle(texture=self.video_texture, pos=self.pos, size=(self.img.shape[1], self.img.shape[0]))
                
    def capture_frame(self):
        try:
            frame = self.picam2.capture_array()
            return frame
        except Exception as e:
            print(f"Error capturing frame: {e}")
            return None
    
    def process_frame(self, frame):
        # Flip the frame horizontally and vertically
        frame = np.fliplr(frame)
        frame = np.flipud(frame)
        # Normalize aspect ratio
        frame = self.normalize_preview_image_aspect_ratio(frame)
        return frame
    
    def normalize_preview_image_aspect_ratio(self, img):
        h = IMAGE_PREVIEW_H
        w = IMAGE_PREVIEW_W
        img = imutils.resize(img, height=h)
        cropx = (img.shape[1] - w)
        if cropx > 0:
            startx = cropx // 2
            img = img[:, startx:startx + w]
        return img
    
    def set_overlay_image(self, i):
        try:
            overlay_path = self.get_overlay_thumbnail_fname(i)
            if overlay_path is None:
                print("Invalid overlay path.")
                self.overlay = None
                return
            overlay = cv2.imread(overlay_path, cv2.IMREAD_UNCHANGED)
            if overlay is None:
                print(f"Failed to load overlay image: {overlay_path}")
                self.overlay = None
                return
            # Resize overlay to match the image size
            overlay = self.normalize_overlay_image_aspect_ratio(overlay)
            # Flip overlay to align with camera feed
            overlay = np.flipud(overlay)
            self.overlay = overlay
            print(f"Overlay set to: {overlay_path}")
        except Exception as e:
            print(f"Error setting overlay image: {e}")
            self.overlay = None
    
    def get_overlay_thumbnail_fname(self, oindex):
        if oindex < 0 or oindex >= len(OVERLAYS):
            print(f"Overlay index {oindex} out of range.")
            return None
        fname = OVERLAYS_DIR + "/" + OVERLAYS[oindex]
        thumbname = "{0}_thumb.{1}".format(*fname.rsplit('.', 1))
        if not os.path.exists(thumbname):
            print(f"Thumbnail not found for overlay: {thumbname}")
            return None
        return thumbname
    
    def normalize_overlay_image_aspect_ratio(self, img):
        img = imutils.resize(img, width=self.img.shape[1], height=self.img.shape[0])
        return img
    
    def apply_overlay(self):
        if self.overlay is not None and self.img is not None:
            y_offset = 0
            x_offset = 0
            y1, y2 = y_offset, y_offset + self.overlay.shape[0]
            x1, x2 = x_offset, x_offset + self.overlay.shape[1]
    
            alpha_s = self.overlay[:, :, 3] / 255.0
            alpha_l = 1.0 - alpha_s
    
            for c in range(0, 3):
                self.img[y1:y2, x1:x2, c] = (alpha_s * self.overlay[:, :, c] +
                                             alpha_l * self.img[y1:y2, x1:x2, c])
    
    def update(self, dt):
        frame = self.capture_frame()
        if frame is not None:
            self.img = self.process_frame(frame)
            self.set_overlay_image(self.overlay_index)
            self.apply_overlay()
            self.buf = self.img.tobytes()
            try:
                self.video_texture.blit_buffer(self.buf, colorfmt='bgr', bufferfmt='ubyte')
                # self.video_texture.flip_vertical()
                self.rect.texture = self.video_texture
            except Exception as e:
                print(f"Error updating texture: {e}")
        else:
            print("Warning: No frame captured during update")
                
    def next_frame(self):
        self.overlay_index += 1
        if self.overlay_index >= len(OVERLAYS):
            self.overlay_index = 0
        self.set_overlay_image(self.overlay_index)
    
    def previous_frame(self):
        self.overlay_index -= 1
        if self.overlay_index < 0:
            self.overlay_index = len(OVERLAYS) - 1
        self.set_overlay_image(self.overlay_index)
        
    def _keyboard_closed(self):
        self._keyboard.unbind(on_key_down=self._on_keyboard_up)
        self._keyboard = None
    
    def _on_keyboard_up(self, keyboard, keycode):
        if keycode[1] == 'left':
            self.previous_frame()
        elif keycode[1] == 'right':
            self.next_frame()
        elif keycode[1] == 'q' or keycode[1] == 'escape':
            raise SystemExit
        return True

class PhotoBooth(ScreenManager):
    def __init__(self, **kwargs):
        super(PhotoBooth, self).__init__(**kwargs)
        
    def start_countdown(self, btn):
        Clock.schedule_once(self.capture, 4)
        btn.disabled=True
        btn.color = (1,1,1,1)
        btn.disabled_color = (1,1,1,1)
        btn.font_size = metrics.sp(32)
        self.change_capture_button_text(btn,"Capturing!")
        cd_phrases = ["Ready!","Steady!","Say\nCheese!"]
        
        num = len(cd_phrases)-1
        def count_it(num):
            if num == len(cd_phrases):
                return
            num += 1
            btn.text = cd_phrases[num-1]
            Clock.schedule_once(lambda dt: count_it(num), 1)

        Clock.schedule_once(lambda dt: count_it(0), 0)
        
        
    def change_capture_button_text(self,btn,t):
        btn.text = t
        
    def normalize_output_image_aspect_ratio(self,img):
        h = OUTPUT_IMG_H
        w = OUTPUT_IMG_W
        img = imutils.resize(img, height=h)
        cropx = (img.shape[1]-w)
        if cropx > 0:
            startx = cropx // 2
            img = img[:, startx:startx + w]
        return img
    
    def normalize_overlay_image_aspect_ratio(self,img):
        img = imutils.resize(img, width=img.shape[1], height=img.shape[0])
        return img
            
    def combine_image_and_overlay(self,img,overlay):
        x_offset=y_offset=0

        y1, y2 = y_offset, y_offset + overlay.shape[0]
        x1, x2 = x_offset, x_offset + overlay.shape[1]

        alpha_s = overlay[:, :, 3] / 255.0
        alpha_l = 1.0 - alpha_s

        for c in range(0, 3):
            img[y1:y2, x1:x2, c] = (alpha_s * overlay[:, :, c] +
                                      alpha_l * img[y1:y2, x1:x2, c])
        
        return img
        
    def capture(self,obj):
        try:
            self.turn_on_led_flash()
        except:
            pass
        sleep(0.5)
        camera = self.ids['camera']
        
        # Capture the current frame
        frame = camera.capture_frame()
        if frame is None:
            print("Error: Failed to capture frame for saving.")
            return
        
        # Process the frame
        img = camera.process_frame(frame)
        
        # Apply overlay
        camera.set_overlay_image(camera.overlay_index)
        camera.apply_overlay()
        
        # Normalize output image aspect ratio
        img = self.normalize_output_image_aspect_ratio(img)
        
        # Generate filename
        timestr = time.strftime("%Y%m%d_%H%M%S")
        fname = f"Images/Photobooth_Img_{camera.overlay_index}_{timestr}.jpg"
        
        # Save the image with overlay
        cv2.imwrite(fname, img, [int(cv2.IMWRITE_JPEG_QUALITY), 90])
        
        # Move to next screen and show the image
        self.goto_photo_edit()
        img_widget = self.ids['taken_photo']
        img_widget.source = fname
        img_widget.update()
        
        try:
            self.turn_off_led_flash()
        except:
            pass
        
    def turn_on_led_flash(self):
        GPIO.output(LED_GPIO_PIN_NUM,GPIO.HIGH)
        
    def turn_off_led_flash(self):
        GPIO.output(LED_GPIO_PIN_NUM,GPIO.LOW)
                
    def reset_capture_button(self):
        self.ids.capture_button.disabled = False
        self.ids.capture_button.text = "Take\nPicture!"
        
    def goto_photo_preview(self):
        self.reset_capture_button()
        self.current = 'photo_preview'

    def goto_photo_edit(self):
        self.reset_capture_button()
        self.current = 'photo_edit'
        
    def goto_saving_dialog(self):
        self.current = 'save_in_progress'
        
    def goto_confirm_saved(self):
        self.reset_capture_button()
        self.current = 'confirm_saved'
        
    def goto_not_saved(self):
        self.reset_capture_button()
        self.current = 'not_saved'
        
    def saved_goto_photo_preview_cb(self,dt):
        self.goto_photo_preview()
        
    def sched_save_to_wordpress(self, btn):
        def callback():
            self.save_to_wordpress(btn)
        t = Thread(target=callback)
        t.start()
        self.goto_saving_dialog()
    
    def save_to_wordpress(self, btn):
        print("Saving to WP!")
        filename = self.ids.taken_photo.source
        img_id = self.upload_image_to_wordpress(filename)
        
        if img_id:
            wp_post = self.create_wordpress_post(img_id)
            
            if wp_post:
                self.goto_confirm_saved()
            else:
                self.goto_not_saved()
        else:
            self.goto_not_saved()
                
    def upload_image_to_wordpress(self, filename):
        client = Client(WP_URI,WP_USER,WP_PWD)
        print("Filename: "+filename)
        data = {
                'name': "Photobooth Image "+"".join(re.findall("\d+", filename))+str(uuid.uuid4())+".png",
                'type': 'image/png',  # mimetype
        }
        
        try:
            # read the binary file and let the XMLRPC library encode it into base64
            with open(filename, 'rb') as img:
                data['bits'] = xmlrpc_client.Binary(img.read())
            
            print("Uploading the image...")
            response = client.call(media.UploadFile(data))
            attachment_id = response['id']
        except Exception as e:
            if isinstance(e, xmlrpc_client.ProtocolError):
                print("Image Couldn't Be Uploaded?")
                print(e)
                nfname = filename.replace("Images/","ErroredImages/")
                shutil.move(filename,nfname)
                return False
            else:
                print("Uh oh!!!")
                print(e)
                return False
        
        return attachment_id

    def create_wordpress_post(self, attachment_id):
        client = Client(WP_URI,WP_USER,WP_PWD)
        try:
            timestr = time.strftime("%Y%m%d_%H%M%S")
            pbimg = WordPressPost()
            pbimg.post_type = 'purikurapi'
            pbimg.title = timestr
            pbimg.content = timestr
            pbimg.thumbnail = attachment_id
            pbimg.post_format = 'image'
            pbimg.post_status = 'publish'

            print("Creating WP Post!")
            post = client.call(posts.NewPost(pbimg))
        except Exception as e:
            if isinstance(e, xmlrpc_client.ProtocolError):
                print("Image Couldn't Be Uploaded?")
                print(e)
                nfname = filename.replace("Images/","ErroredImages/")
                shutil.move(filename,nfname)
                return False
            else:
                print("Uh oh!!!")
                print(e)
                return False
                
        return post
        
class TakenPhoto(Image):
    def __init__(self, **kwargs):
        super(TakenPhoto, self).__init__(**kwargs)
        
    def update(self):
        img = cv2.imread(self.source)
        if img is None:
            print(f"Failed to load image: {self.source}")
            return
        h = IMAGE_PREVIEW_H
        w = IMAGE_PREVIEW_W
        img = np.flipud(img)
        img = self.normalize_output_image_aspect_ratio(img)
        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        buf = img.tobytes()
                
        with self.canvas:
            texture = Texture.create(size=(img.shape[1], img.shape[0]), colorfmt='bgr')
            texture.blit_buffer(buf, colorfmt='bgr', bufferfmt='ubyte')
            rect = Rectangle(texture=texture, pos=self.pos, size=(img.shape[1], img.shape[0]))
        
        
    def normalize_output_image_aspect_ratio(self, img):
        h = IMAGE_PREVIEW_H
        w = IMAGE_PREVIEW_W
        img = imutils.resize(img, height=h)
        cropx = (img.shape[1] - w)
        if cropx > 0:
            startx = cropx // 2
            img = img[:, startx:startx + w]
        return img
            
class PhotoBoothApp(App):
    def build(self):
        return PhotoBooth()

Window.size = WINDOW_RES
Window.show_cursor = config['show_cursor']

PhotoBoothApp().run()
