apt update
apt autoremove -y
apt upgrade -y

apt install build-essential libraspberrypi-dev raspberrypi-kernel-headers -y
apt install software-properties-common -y
apt install python3-opencv -y

apt install -y \
    python-pip \
    build-essential \
    git \
    python \
    python-dev \
    ffmpeg \
    libsdl2-dev \
    libsdl2-image-dev \
    libsdl2-mixer-dev \
    libsdl2-ttf-dev \
    libportmidi-dev \
    libswscale-dev \
    libavformat-dev \
    libavcodec-dev \
    zlib1g-dev

# apt install -y \
#     libgstreamer1.0 \
#     gstreamer1.0-plugins-base \
#     gstreamer1.0-plugins-good

apt install -y \
    libcamera-dev \
    libcap-dev \ 
    libkms++-dev \ 
    libfmt-dev \
    libdrm-dev


# python -m pip install --upgrade pip setuptools virtualenv
#
# add-apt-repository ppa:kivy-team/kivy -y
# apt update
# apt install python3-kivy -y
# apt install kivy-examples -y
#
python3 -m pip install python-wordpress-xmlrpc
python3 -m pip install imutils
python3 -m pip install pyyaml
python3 -m pip install picamera2
python3 -m pip install rpi-libcamera
python3 -m pip install rpi-kms
